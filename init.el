;;; -*- coding: iso-2022-7bit; tab-width: 8 -*-

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; custom
(setq custom-file (concat (getenv "HOME") "/.emacs.d/my-custom.el"))
(load custom-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; package
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; add all of the descendant directories "~/.emacs.d/lisp/" to load-path
(let ((default-directory "~/.emacs.d/lisp/"))
  (normal-top-level-add-subdirs-to-load-path))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; keyboard settings

;; C-h: backspace
;;(keyboard-translate ?\C-h ?\C-?)
;; M-C-/
(define-key esc-map [?\C-_] 'dabbrev-completion)

;; global key map

;; search forward/backword using last C-s search string
(global-set-key "\M-N" 'isearch-repeat-forward)
(global-set-key "\M-P" 'isearch-repeat-backward)

(global-set-key (kbd "<f5>") 'keyboard-escape-quit)
(global-set-key (kbd "<f8>") 'scratch-buffer)
(global-set-key (kbd "<f9>") 'kill-buffer)

;; I sometimes press C-x C-c accidentally, so unbind it
(defun my-dummy-kill-terminal ()
  "dummy `kill-emacs' function.
just show message."
  (interactive)
  (message "%s" "To exit the Emacs, use
  M-x save-buffers-kill-terminal
or
  M-` f==>File q==>Quit"))

(global-set-key (kbd "C-x C-c") 'my-dummy-kill-terminal)

;; I sometimes press C-z accidentally, so unbind it
(defun my-dummy-suspend-frame ()
  "dummy `suspend-frame' function.
just show message."
  (interactive)
  (message "instead of C-z, use C-x C-z to suspend"))
(global-set-key (kbd "C-z") 'my-dummy-suspend-frame)

;; i have used uim
;; (global-unset-key (kbd "C-\\"))         ; toggle-input-method

;; use M-j in IM, default-indent-new-line is still bound to C-M-j
(global-unset-key "\M-j")

;; vim-like window selection
(global-set-key (kbd "C-c h") 'windmove-left)
(global-set-key (kbd "C-c j") 'windmove-down)
(global-set-key (kbd "C-c k") 'windmove-up)
(global-set-key (kbd "C-c l") 'windmove-right)
(global-set-key (kbd "C-c o") 'other-window)

;; ESC C-v is scroll-other-window
(global-set-key (kbd "ESC M-v") 'scroll-other-window-down)

;; unset toggle-input-method (C-\)
;; unset C-\ binding in global
(global-unset-key (kbd "\C-\\"))
;; unset C-\ in isearch
(define-key isearch-mode-map (kbd "\C-\\") nil)
;; default binding
;;(global-set-key (kbd "\C-\\") 'toggle-input-method)
;;(define-key isearch-mode-map (kbd "\C-\\") 'isearch-toggle-input-method)

;; use ibuffer, instead of list-buffers
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; use ffap bindings (C-x C-f , C-x C-r , C-x d , ...)
;; except find-alternate-file (C-x C-v)
(ffap-bindings)
(global-set-key [remap find-alternate-file] nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mouse
(when (not (window-system))
  (xterm-mouse-mode 1)
  (load "mwheel")
  (mouse-wheel-mode 1)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; set ambiguous characters width
;; http://www.unicode.org/ucd/
;; https://www.unicode.org/Public/UCD/latest/ucd/EastAsianWidth.txt
;; (info "(elisp) Char-Tables")
;;
;; set character code (U+0000 .. U+23ff) to width 1
;; adjust to your terminal setting
(set-char-table-range char-width-table '(0 . #x23ff) 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; show minibuffer history
(defun show-minibuffer-history ()
  "Show current minibuffer history in buffer `*minibuffer history*'."
  (interactive)
  (let ((buffer (get-buffer-create "*minibuffer history*")))
    (with-current-buffer buffer
      (erase-buffer)
      (mapc (lambda (x) (insert (format "%s\n" x)))
            (reverse (symbol-value minibuffer-history-variable))))
    (display-buffer buffer)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; smart-tabs-mode

(require 'smart-tabs-mode nil t)
;;(smart-tabs-insinuate 'c 'c++ 'java 'javascript 'cperl 'python 'ruby 'nxml)
(when (fboundp 'smart-tabs-add-language-support)
  (smart-tabs-add-language-support perl perl-mode-hook
    ((perl-indent-line . perl-indent-level)))
  (smart-tabs-add-language-support javascript js-mode-hook
    ((js-indent-line . js-indent-level)))
  (smart-tabs-add-language-support sgml sgml-mode-hook
    ((sgml-indent-line . sgml-basic-offset)))
  (smart-tabs-add-language-support tcl tcl-mode-hook
    ((tcl-indent-line . tcl-indent-level)))
  (smart-tabs-insinuate 'c 'c++ 'javascript 'perl 'cperl 'sgml 'tcl)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; program mode

;; make tab-width and indent-level same value
;; commented out c-basic-offset
;;   some source files require c-basic-offset and tab-width to be different values.
;; (defvaralias 'c-basic-offset 'tab-width)
(defvaralias 'sh-basic-offset 'tab-width)
(defvaralias 'perl-indent-level 'tab-width)
(defvaralias 'tcl-indent-level 'tab-width)
(defvaralias 'ruby-indent-level 'tab-width)
(defvaralias 'js-indent-level 'tab-width)
(defvaralias 'sgml-basic-offset 'tab-width)

;; Customizations for all modes in CC Mode.
(defun my-c-mode-common-hook ()
  "Hook used for $B!F(Bc-mode-common-hook$B!G(B."
  (setq comment-start "// ")
  (setq comment-end "")
  (setq show-trailing-whitespace t)
  )
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; set default style to "linux" instead of "gnu"
(setq c-default-style
      '((java-mode . "java")
        (awk-mode . "awk")
        (other . "linux")))

;; The language specific mode hooks
;; lisp family
(defun my-lisp-mode-hook ()
  "Hook used for lisp-like mode."
  (setq indent-tabs-mode nil))
(add-hook 'lisp-mode-hook 'my-lisp-mode-hook)
(add-hook 'emacs-lisp-mode-hook 'my-lisp-mode-hook)
(add-hook 'scheme-mode-hook 'my-lisp-mode-hook)
;; html
(defun my-html-mode-hook ()
  "Hook used for html-mode$B!G(B."
  (setq sgml-basic-offset 2))
(add-hook 'html-mode-hook 'my-html-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; dired

(defun my-dired-load-hook ()
  "Hook used for `dired-load-hook'."
  (advice-add 'dired-next-line :after #'dired-head-file-function)
  ;;(advice-remove 'dired-next-line #'dired-head-file-function)
  (define-key dired-mode-map "b" 'dired-toggle-head-file)
  (define-key dired-mode-map "\"" 'dired-do-execute)
  )

(add-hook 'dired-load-hook 'my-dired-load-hook)

;; display head of file
(defvar dired-head-file nil
  "Used by `dired-head-file-function'.")

(defvar dired-head-file-byte 1024
  "Byte size of file that display for.")

(defun dired-head-file-function (&rest args)
  "Display head of file.
Optional argument ARGS toggle."
  (if dired-head-file
      (let ((filename (dired-get-filename))
            (buffer (get-buffer-create "*head*"))
            (dir default-directory))
        (with-current-buffer buffer
          (cd dir)
          (setq buffer-read-only nil)
          (erase-buffer)
          (if (file-directory-p filename)
              ;; list directory
              (insert-directory filename "-a" nil t)
            ;; insert head of file content if file size is greater than 0
            (if (< 0 (nth 7 (file-attributes filename)))
                (insert-file-contents filename nil 0 dired-head-file-byte)))
          (goto-char (point-min))
          (view-mode)
          (display-buffer buffer t)
          ))))

(defun dired-toggle-head-file (&optional arg)
  "Toggle display *head*.
if prefix ARG, set `dired-head-file' to t."
  (interactive "P")
  (setq dired-head-file
        (if (null arg)
            (not dired-head-file)
          (> (prefix-numeric-value arg) 0)))
  (if dired-head-file
      (dired-head-file-function)
    (quit-windows-on "*head*")))

(defun dired-do-execute ()
  "Run the file under the cursor as a command."
  (interactive)
  (let* ((cmd (concat (dired-get-filename) " "))
         (arg (read-string "shell command : " cmd )))
    (shell-command arg)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; slime
(setq inferior-lisp-program "sbcl")
;;(setq slime-contribs '(slime-fancy))
(setq slime-contribs '(slime-repl))
;; default http://www.lispworks.com/reference/HyperSpec/
(setq common-lisp-hyperspec-root (concat "file://" (getenv "HOME") "/doc/HyperSpec/"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mark symbol
(defun mark-symbol ()
  "Mark symbol near point.  like `isearch-forward-symbol-at-point'."
  (interactive)
  (let ((bounds (find-tag-default-bounds)))
    (when bounds
      (push-mark (car bounds) nil t)
      (goto-char (cdr bounds)))))
(global-set-key "\C-cs" 'mark-symbol)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; maxima mode
(autoload 'maxima-mode "maxima" "Maxima mode" t)
(autoload 'imaxima "imaxima" "Frontend for maxima with Image support" t)
(autoload 'maxima "maxima" "Maxima interaction" t)
(autoload 'imath-mode "imath" "Imath mode for math formula input" t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; migemo
;;; https://github.com/emacs-jp/migemo
(require 'migemo nil t)
(when (featurep 'migemo)
  ;; cmigemo(default)
  (setq migemo-command "cmigemo")
  (setq migemo-options '("-q" "--emacs"))

  ;; ;; ruby migemo
  ;; (setq migemo-command "ruby")
  ;; (setq migemo-options '("-S" "migemo" "-t" "emacs" "-i" "\a"))

  ;; Set your installed path
  ;; (setq migemo-dictionary "/usr/local/share/migemo/utf-8/migemo-dict")
  (setq migemo-dictionary "/usr/share/cmigemo/utf-8/migemo-dict")

  (setq migemo-user-dictionary nil)
  (setq migemo-regex-dictionary nil)
  (setq migemo-coding-system 'utf-8-unix)
  (migemo-init)
  ;; turn off migemo at startup
  (migemo-isearch-toggle-migemo)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; navi2ch
(when (load "navi2ch-autoloads" t)
    ;; display all responses when opening a thread
    (setq navi2ch-article-auto-range nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; lilypond-mode
(load "lilypond-init" t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; View-mode
(defun my-view-mode-hook ()
  "Hook used for View mode$B!G(B."
  (define-key view-mode-map "j" 'View-scroll-line-forward)
  (define-key view-mode-map "e" 'View-scroll-line-forward)
  (define-key view-mode-map "k" 'View-scroll-line-backward)
  (define-key view-mode-map "f" 'View-scroll-page-forward)
  (define-key view-mode-map "b" 'View-scroll-page-backward))
(add-hook 'view-mode-hook 'my-view-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; add number (vim's ctrl-a)
(require 'thingatpt)

(defun number-to-binary-string (n)
  "return string number in binary from number N"
  (let ((remain n)
        (result '()))
    (while (> n 0)
      (let ((p (/ n 2))
            (q (% n 2)))
        (push (% n 2) result)
        (setq n p)))
    ;; '(1 2 3 4) => "1234"
    (concat (mapcar (lambda (x) (+ x ?0)) result))))

;; this function is based number-at-point in thingatpt.el
(defun my-add-number (amount)
  "add AMOUNT to the number at cursor"
  (interactive "p")
   (cond
    ;; real
    ;; keep number of decimal place
    ((thing-at-point-looking-at "\\(-?[0-9]+\\)\\(\\.\\)\\([0-9]+\\)" 500)
     (let ((place (length (buffer-substring (match-beginning 3) (match-end 3))))
           (real (buffer-substring (match-beginning 0) (match-end 0))))
       (replace-match (format (format "%%.%df" place)
                              (+ (/ amount (expt 10.0 place))
                                 (string-to-number real))))))
    ;; hex
    ((thing-at-point-looking-at "\\(0x\\|#x\\)\\([a-fA-F0-9]+\\)" 500)
     (replace-match (format "%s%x"
                            (buffer-substring (match-beginning 1) (match-end 1))
                            (+ amount
                               (string-to-number
                                (buffer-substring (match-beginning 2) (match-end 2))
                                16)))))
    ;; binary
    ((thing-at-point-looking-at "\\(0b\\|#b\\)\\([01]+\\)" 500)
     (replace-match (format "%s%s"
                            (buffer-substring (match-beginning 1) (match-end 1))
                            (number-to-binary-string
                            (+ amount
                               (string-to-number
                                (buffer-substring (match-beginning 2) (match-end 2))
                                2))))))
    ;; octal
    ((thing-at-point-looking-at "\\(0o\\|#o\\|\\<0\\)\\([0-7]+\\)" 500)
     (replace-match (format "%s%o"
                            (buffer-substring (match-beginning 1) (match-end 1))
                            (+ amount
                               (string-to-number
                                (buffer-substring (match-beginning 2) (match-end 2))
                                8)))))
     ;; integer
    ((thing-at-point-looking-at "-?[0-9]+" 500)
     (replace-match (format "%d"
                            (+ amount
                               (string-to-number
                                (buffer-substring (match-beginning 0) (match-end 0))
                                )))))))

(global-set-key (kbd "M-+") 'my-add-number)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; set background and foreground color together

;; black foreground and white background
(defun black-on-white ()
  (interactive)
  (set-foreground-color "black")
  (set-background-color "white"))

;; white foreground and black background
(defun white-on-black ()
  (interactive)
  (set-foreground-color "white")
  (set-background-color "black"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; browse cve
(defun browse-cve (cve-id)
  "view cve (Common Vulnerabilities and Exposures) url"
  (interactive
   (list
    (when (not (boundp 'cve-id))
      (save-excursion
        (let ((s-end (progn (backward-char) (forward-symbol 1) (point)))
              (s-begin (progn (forward-symbol -1) (point))))
          (buffer-substring-no-properties s-begin s-end))))))
  ;; (browse-url (concat "https://cve.mitre.org/cgi-bin/cvename.cgi?name=" cve-id))
  (browse-url (concat "https://nvd.nist.gov/vuln/detail/" cve-id))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; browse debian bugs
(defun browse-debian-bugs (id)
  "view debian bug url"
  (interactive
   (list
    (when (not (boundp 'id))
      (save-excursion
        (let ((s-end (progn (backward-char) (forward-word 1) (point)))
              (s-begin (progn (forward-word -1) (point))))
          (buffer-substring-no-properties s-begin s-end))))))
  (browse-url (concat "https://bugs.debian.org/" id))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; enable disabled command
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'narrow-to-region 'disabled nil)
