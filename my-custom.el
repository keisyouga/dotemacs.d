(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LilyPond-all-midi-command "timidity -ik -OsM")
 '(LilyPond-pdf-command "mupdf")
 '(TeX-PDF-from-DVI "Dvipdfmx")
 '(TeX-parse-self t)
 '(TeX-view-program-selection
   '(((output-dvi has-no-display-manager) "dvi2tty")
     ((output-dvi style-pstricks) "dvips and gv") (output-dvi "xdvi")
     (output-pdf "MuPDF") (output-html "xdg-open")))
 '(async-shell-command-buffer 'rename-buffer)
 '(auto-save-default nil)
 '(backup-directory-alist '(("." . "~/.emacs.d/backups")))
 '(browse-url-browser-function 'eww-browse-url)
 '(column-number-mode t)
 '(completion-cycle-threshold t)
 '(create-lockfiles nil)
 '(delete-active-region nil)
 '(delete-old-versions t)
 '(desktop-modes-not-to-save
   '(tags-table-mode dired-mode Info-mode vc-dir-mode eww-mode))
 '(dired-guess-shell-alist-user
   '(("\\.c\\'" "cc -Wall -g" "tcc -run") ("\\.cc\\'" "c++ -Wall -g")
     ("\\.C\\'" "c++ -Wall -g") ("\\.cpp\\'" "c++ -Wall -g")
     ("\\.cxx\\'" "c++ -Wall -g") ("\\.c\\+\\+\\'" "c++ -Wall -g")
     ("\\.pl\\'" "perl") ("\\.py\\'" "python") ("\\.rb\\'" "ruby")
     ("\\.lua\\'" "lua") ("\\.tcl\\'" "tclsh" "wish")
     ("\\.lisp\\'" "sbcl --script" "sbcl --load")
     ("\\.scm\\'" "scheme") ("\\.png\\'" "feh" "sxiv")
     ("\\.gif\\'" "feh" "sxiv") ("\\.jpe?g\\'" "feh" "sxiv")
     ("\\.xpm\\'" "feh" "sxiv") ("\\.bmp\\'" "feh" "sxiv")
     ("\\.svg\\'" "inkview *" "display *")
     ("\\.pdf\\'" "mupdf" "zathura") ("\\.epub\\'" "mupdf")
     ("\\.dot\\'" "dot -Tpng * | feh -") ("\\.mp4\\'" "mpv")
     ("\\.avi\\'" "mpv") ("\\.mpg\\'" "mpv") ("\\.wmv\\'" "mpv")
     ("\\.mkv\\'" "mpv") ("\\.flv\\'" "mpv") ("\\.webm\\'" "mpv")
     ("\\.mov\\'" "mpv") ("\\.qt\\'" "mpv") ("\\.3gp\\'" "mpv")
     ("\\.el\\'" "emacs --script" "emacs -batch -f batch-byte-compile")
     ("\\.elc\\'" "emacs --script")
     ("\\.abc\\'" "abcm2ps -g -O- * | display -" "abc2midi")
     ("\\.midi?\\'" "timidity -ik -OsM *")))
 '(electric-pair-mode t)
 '(global-company-mode t)
 '(global-hl-line-mode t)
 '(global-whitespace-mode t)
 '(grep-save-buffers nil)
 '(ibuffer-formats
   '((mark modified read-only locked " " (name 25 25 :left :elide) " "
           (size 9 -1 :right) " " (mode 16 16 :left :elide) " "
           filename-and-process)
     (mark " " (name 16 -1) " " filename)))
 '(inhibit-startup-screen t)
 '(line-number-mode t)
 '(make-backup-files t)
 '(markdown-command "cmark-gfm -e table")
 '(next-line-add-newlines t)
 '(package-selected-packages
   '(abc-mode auctex company elpher graphviz-dot-mode lua-mode
              markdown-mode migemo pinyin slime smart-tabs-mode
              yascroll yasnippet yasnippet-snippets))
 '(preview-image-type 'dvipng)
 '(query-about-changed-file nil)
 '(read-buffer-completion-ignore-case t)
 '(read-file-name-completion-ignore-case t)
 '(safe-local-variable-values
   '((etags-regen-ignores "test/manual/etags/")
     (etags-regen-regexp-alist
      (("c" "objc") "/[ \11]*DEFVAR_[A-Z_ \11(]+\"\\([^\"]+\\)\"/\\1/"
       "/[ \11]*DEFVAR_[A-Z_ \11(]+\"[^\"]+\",[ \11]\\([A-Za-z0-9_]+\\)/\\1/"))))
 '(savehist-mode nil)
 '(show-paren-mode t)
 '(transient-mark-mode nil)
 '(vc-follow-symlinks t)
 '(version-control t)
 '(view-read-only t)
 '(which-function-mode t)
 '(which-key-mode t)
 '(whitespace-style '(face trailing tabs spaces missing-newline-at-eof))
 '(yas-global-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Man-overstrike ((t (:inherit bold :foreground "red"))))
 '(Man-underline ((t (:inherit underline :foreground "green3"))))
 '(whitespace-space ((((class color) (background light)) (:background "#eeeeff" :foreground "#0080e0")) (((background dark)) (:background "#222222" :foreground "#8080c0"))))
 '(whitespace-tab ((((class color) (background light)) (:background "#eeddff" :foreground "#00e080" :underline t)) (((class color) (background dark)) (:background "#1f1f1f" :foreground "#008040" :underline t)))))

